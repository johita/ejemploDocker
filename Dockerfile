#Imagen Base
FROM node:8

#descripcion
LABEL Author="Johanna Sanchez" 

#creacion de variables de entorn ENV(nombre, valor)
ENV NODEAPP /nodeapp

#Crear carpeta
RUN mkdir  ${NODEAPP}

#Definir el directorio actual
WORKDIR ${NODEAPP}/

#Copiar (fuente hacia el destino)
COPY package.json ${NODEAPP}

#Instalar dependencias
RUN npm install

#copiar 
COPY . ${NODEAPP}

#expone el puerto 80(puertoHost:puertoContenedor)
#expone el puerto 8000 del contenedor
EXPOSE 8080

#Define el comando final que se ejecutará
CMD [ "npm", "start" ]